# STM32+W5500+aliyun

#### 介绍
基于STM32F407+W5500的硬件系统，通过MQTT协议控制阿里云上的智能设备

#### 软件架构
软件架构说明
软件：KEIL v5.30
操作系统：FREERTOS
HAL库生成软件：STM32CubeMX V6.2.1

#### 使用说明
1.  在阿里云创建设备
![输入图片说明](https://images.gitee.com/uploads/images/2021/0823/093551_0a3d7c64_2028422.png "屏幕截图.png")
2.  首先将W5500通过网线和路由器直接连接，然后程序通过DHCP自动获取IP地址
3.  串口接收PC发来的信息，直接通过W5500以MQTT报文形式PUBLISH到阿里云 
    - 串口可以发布的信息有：
    -         单个："PowerSwitch":0
    -         多个："CurrentHumidity":42,"CurrentTemperature":46.65,"PowerSwitch":0
4.  MQTT协议代码部分是我自己写的，功能很单一，只有CONNECT、CONNACK、PUBLISH、PING

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
