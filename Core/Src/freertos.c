/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdio.h>
#include <string.h>
#include "usart.h"
#include "dhcp.h"
#include "mqtt.h"
#include "w5500.h"
#include "socket.h"
#include "tcp_demo.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
extern MODBUS_InitTypeDef UART4RxStruct;
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */



/* USER CODE END Variables */
osThreadId defaultTaskHandle;
osThreadId dhcpTaskHandle;
osThreadId mqttTaskHandle;

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void StartDefaultTask(void const * argument);
void DHCP_Task(void const * argument);
void MQTT_Task(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* GetIdleTaskMemory prototype (linked to static allocation support) */
void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize );

/* USER CODE BEGIN GET_IDLE_TASK_MEMORY */
static StaticTask_t xIdleTaskTCBBuffer;
static StackType_t xIdleStack[configMINIMAL_STACK_SIZE];

void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize )
{
  *ppxIdleTaskTCBBuffer = &xIdleTaskTCBBuffer;
  *ppxIdleTaskStackBuffer = &xIdleStack[0];
  *pulIdleTaskStackSize = configMINIMAL_STACK_SIZE;
  /* place for user code */
}
/* USER CODE END GET_IDLE_TASK_MEMORY */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of defaultTask */
  osThreadDef(defaultTask, StartDefaultTask, osPriorityNormal, 0, 128);
  defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

  /* definition and creation of dhcpTask */
  osThreadDef(dhcpTask, DHCP_Task, osPriorityIdle, 0, 128);
  dhcpTaskHandle = osThreadCreate(osThread(dhcpTask), NULL);

  /* definition and creation of mqttTask */
  osThreadDef(mqttTask, MQTT_Task, osPriorityIdle, 0, 128);
  mqttTaskHandle = osThreadCreate(osThread(mqttTask), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

}

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void const * argument)
{
  /* USER CODE BEGIN StartDefaultTask */

  uint8_t PINGtime=0;
	uint8_t txbuff[100];
  /* Infinite loop */
  for(;;)
  {
    HAL_GPIO_TogglePin(LED4_R_GPIO_Port, LED4_R_Pin);
    osDelay(500);
    
    if(UART4RxStruct.RxFinish==1)
		{ 
			UART4RxStruct.RxFinish = 0;
      PINGtime=0;
			memcpy(txbuff,UART4RxStruct.RxBuff,UART4RxStruct.RxLength);
			memset(UART4RxStruct.RxBuff,0,sizeof(UART4RxStruct.RxBuff));
      sendPUBLISH(txbuff);
			memset(txbuff,0,sizeof(txbuff));
    }
    
    if(++PINGtime>60)
    {
      PINGtime=0;
      sendPING();
      osDelay(50);
      readPING();
    }
  }
  /* USER CODE END StartDefaultTask */
}

/* USER CODE BEGIN Header_DHCP_Task */
extern uint8_t dhcp_state;
/**
* @brief Function implementing the dhcpTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_DHCP_Task */
void DHCP_Task(void const * argument)
{
  /* USER CODE BEGIN DHCP_Task */

//  uint8_t ss=0,mm=0,hh=0;
  /* Infinite loop */
  for(;;)
  {
    do_dhcp();
    if(dhcp_state==STATE_DHCP_LEASED)
    {
      osDelay(6000);//延时60s
    }
    else
    {
      HAL_GPIO_TogglePin(LED6_R_GPIO_Port, LED6_R_Pin);
      osDelay(100);
    }
      
  }
  /* USER CODE END DHCP_Task */
}

/* USER CODE BEGIN Header_MQTT_Task */
/**
* @brief Function implementing the mqttTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_MQTT_Task */
void MQTT_Task(void const * argument)
{
  /* USER CODE BEGIN MQTT_Task */
  while(do_tcp_client()!=1)
  {
    HAL_GPIO_TogglePin(LED6_R_GPIO_Port, LED6_R_Pin);
    printf("== while(do_tcp_client()!=1)==  \r\n");
    osDelay(500);
  }       	/*循环执行的函数*/ 
  
  if(getSn_IR(SOCK_TCPC) & Sn_IR_CON)
  {				
    setSn_IR(SOCK_TCPC, Sn_IR_CON); 			/*清除接收中断标志位，如果想把 Sn_IR 位清零的话，主机应该将该位置‘1’。*/
  }
  
  //CONNECT
  sendCONNECT();
  
  while((getSn_IR(SOCK_TCPC) & Sn_IR_RECV)==0) 				// Sn_IR的RECV位置1，通知W5500收到数据
  {
    printf(" 等待阿里云响应 ...\r\n");
    osDelay(100);  
  }				
  setSn_IR(SOCK_TCPC, Sn_IR_RECV);			
  
  //CONNACK
  switch(readCONNACK())
  {
  case 0: printf("连接已接受\r\n"); break;//连接已被服务端接受
  case 1: printf("连接已拒绝，不支持的协议版本\r\n"); break;//服务端不支持客户端请求的 MQTT 协议级别
  case 2: printf("连接已拒绝，不合格的客户端标识符\r\n"); break;//客户端标识符是正确的 UTF-8 编码， 但服务端不允许使用
  case 3: printf("连接已拒绝，服务端不可用 \r\n"); break;//网络连接已建立， 但 MQTT 服务不可用
  case 4: printf("连接已拒绝，无效的用户名或密码\r\n"); break;//用户名或密码的数据格式无效
  case 5: printf("连接已拒绝，未授权\r\n"); break;//未授权 客户端未被授权连接到此服务器
  case 0xff: printf("bug!!\r\n"); break;//未授权 客户端未被授权连接到此服务器
  default: printf("保留\r\n"); break;//
  }
  
  
  /* Infinite loop */
  for(;;)
  {
//    sendPING();
//    osDelay(500);
//    readPING();
    osDelay(5000);//延时50s
  }
  /* USER CODE END MQTT_Task */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */

/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
