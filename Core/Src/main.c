/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"
#include "spi.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
/**@brief：本程序的功能：串口接收PC发来的信息，直接通过W5500以MQTT报文形式PUBLISH到阿里云
  *@串口可以发布的信息有：
  *@单个："PowerSwitch":0
	*@多个："CurrentHumidity":42,"CurrentTemperature":46.65,"PowerSwitch":0
*/
#include "w5500.h"
#include "W5500_conf.h"
#include "socket.h"
#include "utility.h"
#include "tcp_demo.h"

#include "dhcp.h"
#include "mqtt.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

uint8_t uart4RXbuff=0;//没啥用，只是用来启动485接收

uint8_t UART4Rx=0;

MODBUS_InitTypeDef UART4RxStruct;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void MX_FREERTOS_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
/*************************************************************************
*程序名称    :   Receive_Data_Handler
*功能        :   接收数据处理程序
*@输入参数   :   receivedata——接收到的数据
*@主调函数   :   串口中断RS485_USART_IRQHandler
*************************************************************************/
void Receive_Data_Handler(uint8_t receivedata)
{
  // 设置接收标志 
  UART4RxStruct.RxFlag  = 1;//接收标志
  UART4RxStruct.RxTimer = 0;
  
  //如果该数据是首数据（第一个字节总是从机地址）
  if( UART4RxStruct.RxAddrAck==0  ){ 
    {//且是本机地址或是广播的
      UART4RxStruct.RxAddrAck  = 1;//表示已经确认
    }	
  } 	
  UART4RxStruct.RxBuff[UART4RxStruct.RxIndicator++] = receivedata;	
}
/*************************************************************************
*程序名称    :   Receive_Finish_Handler
*功能        :   接收完成处理程序
*@主调函数   :   系统时钟SysTick_Handler，1ms调用一次
*************************************************************************/
void Receive_Finish_Handler(void)
{
  if((UART4RxStruct.RxFlag!=0))//接收标志,在串口中断函数中置1,表示串口开始接收数据了
  {    
    UART4RxStruct.RxTimer++;//时间间隔计数器，SysTick每1ms中断一次
    if( UART4RxStruct.RxTimer > 10 ) //通讯计时器,时间间隔100ms，说明后面是新数据
    {	
      UART4RxStruct.RxFlag = 0;//接收标志清零
      UART4RxStruct.RxTimer     = 0;//时间间隔计数器清零      
      UART4RxStruct.RxAddrAck   = 0;//下次重新确认地址	
      UART4RxStruct.RxLength = UART4RxStruct.RxIndicator;//下一个数据存放地址
      UART4RxStruct.RxIndicator = 0;//下次重头开始放
      UART4RxStruct.RxFinish    = 1;//当前数据接收完毕
    }
  }
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
  if(huart->Instance==UART4)
  {
//    sendPUBLISH("PowerSwitch",uartRXbuff+0x30);
    HAL_GPIO_TogglePin(LED6_R_GPIO_Port, LED6_R_Pin);
    Receive_Data_Handler(uart4RXbuff);//读取接收的数据	
    HAL_UART_Receive_IT(&huart4,&uart4RXbuff,1);//继续接收
    
  }  
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
  uint8_t version=0;
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_SPI3_Init();
  MX_TIM2_Init();
  MX_UART4_Init();
  /* USER CODE BEGIN 2 */

  reset_w5500();											/*硬复位W5500*/
  wiz_read_buf(VERSIONR, &version, 1);//读W5500的版本号，默认为 0x04。
  printf(" VERSIONR= %d. \r\n",version); 
  if(version==0x04)
  {
    printf(" 初始化MCU相关引脚成功 \r\n"); 
  }
  else
  {		
    printf(" 初始化MCU相关引脚失败 \r\n"); 
    printf(" 请按下复位键，重启系统\r\n"); 
    while(1);	//
  }
  set_w5500_mac();										/*配置MAC地址*///本机物理地址寄存器（SHAR）
  
  socket_buf_init(txsize, rxsize);		/*初始化8个Socket的发送接收缓存大小*/
  
  
  printf(" PC作为TCP服务器，W5500作为TCP客户端\r\n");
  printf(" MQTT服务器IP： %d.%d.%d.%d \r\n",remote_ip[0],remote_ip[1],remote_ip[2],remote_ip[3]); 
  printf(" MQTT监听端口为：%d \r\n",remote_port);
  
  
  while(do_dhcp()!=DHCP_RET_UPDATE)
  {
    HAL_GPIO_TogglePin(LED6_R_GPIO_Port, LED6_R_Pin);
#ifdef _DHCP_DEBUG
    printf("== while(do_dhcp()!=DHCP_RET_UPDATE)==  \r\n");
#endif  
    HAL_Delay(200);
  }       	/*循环执行的函数*/ 
  printf(">>> DHCP获取IP地址成功\r\n");
  
  HAL_UART_Receive_IT(&huart4,&uart4RXbuff,1);//开启485接收
  
  /* USER CODE END 2 */

  /* Call init function for freertos objects (in freertos.c) */
  MX_FREERTOS_Init();
  /* Start scheduler */
  osKernelStart();

  /* We should never get here as control is now taken by the scheduler */
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 4;
  RCC_OscInitStruct.PLL.PLLN = 168;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

 /**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM3 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM3) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */
  //TIM2=1ms调用一次
  if (htim->Instance == TIM2) {
    timer2_isr();
		Receive_Finish_Handler();
  }
  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
