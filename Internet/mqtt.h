#ifndef __MQTT_H
#define	__MQTT_H

#include "main.h"
#include "W5500_conf.h"

void sendCONNECT(void);
uint8_t readCONNACK(void);
void sendPING(void);
uint8_t readPING(void);
void sendSUBSCRIBE(void);
uint8_t recvPUBLISH(void);
//void sendPUBLISH(uint8_t *param,uint8_t* bitstate);
void sendPUBLISH(uint8_t param[0xff]);

#endif /* __MQTT_H */
