#include "mqtt.h"
#include <stdio.h>
#include <string.h>
#include "tcp_demo.h"
#include "W5500_conf.h"
#include "w5500.h"
#include "socket.h"
#include "utility.h"

#define _MQTT_DEBUG	

 uint8_t ClientID[10]="D001";//const——不准更改
 uint8_t ProductKey[50]="a1tN8jvX8cW";
 uint8_t DeviceName[10]="D001";


uint8_t fillStr(uint8_t *dst,uint8_t *source, uint8_t *arg1,uint8_t *arg2)
{
	uint8_t i_dst=0,i_source=0,i_arg=0;//uChar=0,
	uint8_t *p_arg,*p_dst,*p_source;
	p_dst=dst;
	p_source=source;
	p_arg=arg1;
	
//	uChar=strlen((char*)arg1);
//	p_source = (uint8_t *)strstr((char*)source, "%s")+2;
	while((p_source[i_source]!=0)&(p_source[i_source]>0x20)&(p_source[i_source]<0x80))
	{
		if(p_source[i_source]!='%')
		{
			p_dst[i_dst++]=p_source[i_source++];
		}
		else
		{
			while(p_arg[i_arg]!=0)
			{
				p_dst[i_dst++]=p_arg[i_arg++];
			}
			if(*arg2!=NULL)
			{
				i_source=i_source+2;
				i_arg=0;
				p_arg=arg2;
			}
		}
	}
	//for(i=0;i<uChar;i++)
	p_dst[i_dst]=0;
	printf("i_dst=%d",i_dst);
	return i_dst;
}
void sendCONNECT(void)
{
  uint8_t i=0,nbytes=0;
  //  const uint8_t connect_buffer[0xff]={
  //    0x10,
  //    0x6E,0x00,0x04,0x4D,0x51,0x54,0x54,0x04,0xC2,0x00,
  //    0x64,0x00,0x26,0x44,0x30,0x30,0x31,0x7C,0x73,0x65,
  //    0x63,0x75,0x72,0x65,0x6D,0x6F,0x64,0x65,0x3D,0x33,
  //    0x2C,0x73,0x69,0x67,0x6E,0x6D,0x65,0x74,0x68,0x6F,
  //    0x64,0x3D,0x68,0x6D,0x61,0x63,0x73,0x68,0x61,0x31,
  //    0x7C,0x00,0x10,0x44,0x30,0x30,0x31,0x26,0x61,0x31,
  //    0x74,0x4E,0x38,0x6A,0x76,0x58,0x38,0x63,0x57,0x00,
  //    0x28,0x66,0x34,0x36,0x66,0x39,0x35,0x64,0x65,0x30,
  //    0x35,0x30,0x65,0x33,0x36,0x33,0x63,0x38,0x65,0x65,
  //    0x36,0x66,0x61,0x32,0x64,0x36,0x65,0x64,0x30,0x36,
  //    0x31,0x38,0x63,0x32,0x39,0x63,0x63,0x61,0x36,0x33,
  //    0x39 
  //  };	
  uint8_t connect_buffer[0xff];
  uint8_t *p;
  
  uint8_t  FixedHeader=0x10;
  //最大是4个字节，但是一般一个字节——127，就够了
  uint8_t  RemiLength=0;//这里最大是127
  
  uint8_t ProtocolName[10]="MQTT";
  //  uint16_t NameLen=sizeof(ProtocolName)-1;//sizeof是占得字节数（注意字符串是以“0x00结尾”，所以sizeof(ProtocolName)=5），strlen是字符串长度，strlen(ProtocolName)=4
  uint8_t  ProtocolVersion=4;
  uint8_t  ConnectFlags=0xC2;//bit7=User Name Flag,bit6=Flag,bit1=Clean Session
  uint16_t KeepAlive=100;
  
  uint8_t  ClientIdentifier[100];//="D001|securemode=3,signmethod=hmacsha1|";//${ClientID}|securemode=${Mode},signmethod=${SignMethod}| 
  
  uint8_t  UserName[100];//="D001&a1tN8jvX8cW";//${DeviceName}&${ProductKey} 
  
  uint8_t  Password[100]="f46f95de050e363c8ee6fa2d6ed0618c29cca639";
  
  sprintf((char*)ClientIdentifier,"%s|securemode=3,signmethod=hmacsha1|",ClientID);//ClientID,Mode,SignMethod
  sprintf((char*)UserName,"%s&%s",DeviceName,ProductKey);//DeviceName,ProductKey
  
  RemiLength=strlen((char*)ProtocolName)+2+sizeof(ProtocolVersion)+sizeof(ConnectFlags)+sizeof(KeepAlive)+strlen((char*)ClientIdentifier)+2+strlen((char*)UserName)+2+strlen((char*)Password)+2;
  
  
  i=0;
  p=connect_buffer;
  
  p[i++]=FixedHeader;
  p[i++]=RemiLength;
  p[i++]=(uint8_t)(strlen((char*)ProtocolName)>>8);
  p[i++]=(uint8_t)(strlen((char*)ProtocolName)&0xff);
  i+=sprintf((char*)(p+i),"%s",(char*)ProtocolName);//Protocol Name
  p[i++]=ProtocolVersion;
  p[i++]=ConnectFlags;
  p[i++]=(uint8_t)(KeepAlive>>8);
  p[i++]=(uint8_t)(KeepAlive&0xff);
  
  p[i++]=(uint8_t)(strlen((char*)ClientIdentifier)>>8);
  p[i++]=(uint8_t)(strlen((char*)ClientIdentifier)&0xff);
  i+=sprintf((char*)(p+i),"%s",(char*)ClientIdentifier);//Client Identifier
  
  p[i++]=(uint8_t)(strlen((char*)UserName)>>8);
  p[i++]=(uint8_t)(strlen((char*)UserName)&0xff);
  i+=sprintf((char*)(p+i),"%s",(char*)UserName);//User Name
  
  p[i++]=(uint8_t)(strlen((char*)Password)>>8);
  p[i++]=(uint8_t)(strlen((char*)Password)&0xff);
  i+=sprintf((char*)(p+i),"%s",(char*)Password);//Password
  
  nbytes=i;
  
  
  
  
  send(SOCK_TCPC,connect_buffer,nbytes);//strlen((char *)connect_buffer),不能用strlen，strlen到0x00就结束了，所以strlen((char *)connect_buffer)=2
#ifdef _MQTT_DEBUG	  
	printf("MQTT CONNECT send data=\r\n");
  for(i=0;i<nbytes;i++)//i<sizeof(connect_buffer)
    printf("%02X ",connect_buffer[i]);
  printf("\r\n");
#endif
}

uint8_t readCONNACK(void)
{
  uint8_t buff[0xff],i=0;	
  uint8_t ret=0xff;
  uint16_t len=0;
  
  len=getSn_RX_RSR(SOCK_TCPC); 		/*定义len为已接收数据的长度*/  
  
  if(len>0)
  {
    memset(buff,0,sizeof(buff));				// 串口接收缓存清0
    recv(SOCK_TCPC,buff,len); 				/*接收来自Server的数据*/
    //buff[len]=0x00;  					/*添加字符串结束符*/
    for(i=0;i<len;i++)
      printf("%02X-",buff[i]);
    printf("\r\n");
    ret=buff[3];//返回确认码
  }	
  return ret;
}
void sendPING(void)
{
  //	uint8_t i=0;
  uint8_t ping_buffer[2]={0xC0,0x00};
  send(SOCK_TCPC,ping_buffer,2);//strlen((char *)aliyun_buffer)
  printf("send PING= %02X-%02X \r\n",ping_buffer[0],ping_buffer[1]);
}

uint8_t readPING(void)
{
  uint8_t buff[20];	
  //	uint8_t ret=0xff;//返回0——表示一切OK，返回0xff——表示有问题
  uint16_t len=0;
  
  len=getSn_RX_RSR(SOCK_TCPC); 			/*定义len为已接收数据的长度*/  
  
  if(len>0)
  {
    memset(buff,0,sizeof(buff));					// 串口接收缓存清0
    recv(SOCK_TCPC,buff,len); 					/*接收来自Server的数据*/
    
    printf("> recv PING= %02X-%02X\r\n",buff[0],buff[1]);
    if(buff[0]!=0xD0)
      printf("收到的不是PING的回应！\r\n");
    if(buff[1]!=0)
      printf("PINGRESP的剩余长度不是0！\r\n");
    
    return 0;//返回确认码
  }	
  return 0xff;
}
void sendSUBSCRIBE(void)
{
  
  uint8_t i=0;
  const uint8_t subscribe_buffer[200]={
    0x82,
    0x3E,0x00,0xA0,0x00,0x39,0x2F,0x73,0x79,0x73,0x2F,
    0x61,0x31,0x74,0x4E,0x38,0x6A,0x76,0x58,0x38,0x63,
    0x57,0x2F,0x24,0x7B,0x64,0x65,0x76,0x69,0x63,0x65,
    0x4E,0x61,0x6D,0x65,0x7D,0x2F,0x74,0x68,0x69,0x6E,
    0x67,0x2F,0x73,0x65,0x72,0x76,0x69,0x63,0x65,0x2F,
    0x70,0x72,0x6F,0x70,0x65,0x72,0x74,0x79,0x2F,0x73,
    0x65,0x74,0x00
  };
  send(SOCK_TCPC,subscribe_buffer,112);//strlen((char *)aliyun_buffer)
  printf("send SUBSCRIBE=\r\n");
  for(i=0;i<strlen((char *)subscribe_buffer);i++)
    printf("%02X ",subscribe_buffer[i]);
  printf("\r\n");
}
uint8_t recvPUBLISH(void)
{
  uint8_t buff[0xff],i=0;	
  uint8_t ret=0xff;//返回0——表示一切OK，返回0xff——表示有问题
  uint16_t len=0;
  
  len=getSn_RX_RSR(SOCK_TCPC); 					/*定义len为已接收数据的长度*/  
  
  if(len>0)
  {
    if(len<150)
      return 0xff;
    
    memset(buff,0,len+1);					// 串口接收缓存清0
    recv(SOCK_TCPC,buff,len); 					/*接收来自Server的数据*/
    
    printf("recv PUBLISH=");
    for(i=0;i<len;i++)
    {
      printf("%02X ",buff[i]);
    }
    printf("\r\n");
    
    if(buff[0]!=0x30)
      printf("收到的不是PUBLISH的命令！\r\n");
    
    ret=buff[134]-0x30;
    printf("recv PUBLISH的是：%d！\r\n",ret);
    return ret;//返回
  }	
  return 0xff;
}

uint32_t packetID=1;
uint8_t publish_buffer[0xff];
uint8_t TopicName[0xff];//;="/sys/a1tN8jvX8cW/D001/thing/event/property/post";  
uint8_t Payload[0xff];//;="{\"method\":\"thing.event.property.post\",\"id\":\"000000007\",\"params\":{\"PowerSwitch\":0},\"version\":\"1.0.0\"}";
//sendPUBLISH("\"PowerSwitch\":1")
//sendPUBLISH("\"CurrentHumidity\":62")
//sendPUBLISH("\"CurrentTemperature\":29.65")
void sendPUBLISH(uint8_t param[0xff])
{
  
  uint8_t i=0,nbytes=0;
  uint8_t *p;
  
  uint8_t  FixedHeader=0x30;  
  uint16_t  RemiLength=0;//最大是4个字节，但是一般2个字节——16,384，够了
	
  memset(publish_buffer,0,sizeof(publish_buffer));
	memset(TopicName,0,sizeof(TopicName));
	memset(Payload,0,sizeof(Payload));
	
	
#ifdef _MQTT_DEBUG
	printf("ProductKey=%s,strlen(ProductKey)=%d\r\n",ProductKey,strlen((char*)ProductKey));
	printf("DeviceName=%s,strlen(DeviceName)=%d\r\n",DeviceName,strlen((char*)DeviceName));
	printf("param=%s,strlen(param)=%d\r\n",param,strlen((char*)param));
#endif  	  
  
  sprintf((char*)TopicName,"/sys/a1tN8jvX8cW/D001/thing/event/property/post");
#ifdef _MQTT_DEBUG	
  printf("strlen(TopicName)=%d\r\n",strlen((char*)TopicName));
  printf("MQTT PUBLISH TopicName=%s\r\n",TopicName);
#endif  	

	i=sprintf((char*)Payload,"{\"method\":\"thing.event.property.post\",\"id\":\"%09d\",\"params\":{%s},\"version\":\"1.0.0\"}",packetID++,param);
#ifdef _MQTT_DEBUG	
	printf("strlen(Payload)=%d\r\n",strlen((char*)Payload));
  printf("MQTT PUBLISH Payload=%s\r\n",Payload);
#endif  	


	
  if(packetID>428499470)packetID=1;
  
  RemiLength=strlen((char*)TopicName)+2+strlen((char*)Payload);
  
  i=0;
	p=publish_buffer;
  
  p[i++]=FixedHeader;
  if(RemiLength>127)
  {
    p[i++]=RemiLength%128+0x80;
    p[i++]=RemiLength/128;
  }
  else
    p[i++]=RemiLength;
  
  p[i++]=(uint8_t)(strlen((char*)TopicName)>>8);
  p[i++]=(uint8_t)(strlen((char*)TopicName)&0xff);
  i+=sprintf((char*)(p+i),"%s%s",TopicName,Payload);//Topic Name
  printf("TopicName+Payload i=%d\r\n",i);
  
//  i+=sprintf((char*)(p+i),"%s",(char*)Payload);//Payload
//  printf("Payload i=%d\r\n",i);
  
  nbytes=i;
  
  
  for(i=0;i<nbytes;i++)
    printf("%02X ",publish_buffer[i]);
  printf("\r\n");
  
  send(SOCK_TCPC,publish_buffer,nbytes);//strlen((char *)aliyun_buffer)
}



