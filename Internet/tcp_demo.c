/**
************************************************************************************************
* @file   		tcp_demo.c
* @author  		WIZnet Software Team 
* @version 		V1.0
* @date    		2015-02-14
* @brief   		TCP 演示函数
* @attention  
************************************************************************************************
**/

#include <stdio.h>
#include <string.h>
#include "tcp_demo.h"
#include "W5500_conf.h"
#include "w5500.h"
#include "socket.h"
#include "utility.h"
#include "mqtt.h"


uint8_t buff[2048];				                              	         /*定义一个2KB的缓存*/

/**
*@brief		TCP Server回环演示函数。
*@param		无
*@return	无
*/
void do_tcp_server(void)
{	
	uint16_t len=0;  
	switch(getSn_SR(SOCK_TCPS))											            	/*获取socket的状态*/
	{
		case SOCK_CLOSED:													                  /*socket处于关闭状态*/
			printf(" socket的状态 = SOCK_CLOSED\r\n");
			socket(SOCK_TCPS ,Sn_MR_TCP,local_port,Sn_MR_ND);	        /*打开socket*/
		  break;     
    
		case SOCK_INIT:														                  /*socket已初始化状态*/
			printf(" socket的状态 = SOCK_INIT\r\n");
			listen(SOCK_TCPS);												                /*socket建立监听*/
		  break;
		
		case SOCK_ESTABLISHED:												              /*socket处于连接建立状态*/		
			printf(" socket的状态 = SOCK_ESTABLISHED\r\n");
			if(getSn_IR(SOCK_TCPS) & Sn_IR_CON)
			{
				setSn_IR(SOCK_TCPS, Sn_IR_CON);								          /*清除接收中断标志位*/
			}
			len=getSn_RX_RSR(SOCK_TCPS);									            /*定义len为已接收数据的长度*/
			if(len>0)
			{
				recv(SOCK_TCPS,buff,len);								              	/*接收来自Client的数据*/
				buff[len]=0x00; 											                  /*添加字符串结束符*/
				printf("%s\r\n",buff);
				send(SOCK_TCPS,buff,len);									              /*向Client发送数据*/
		  }
		  break;
		
		case SOCK_CLOSE_WAIT:												                /*socket处于等待关闭状态*/
			printf(" socket的状态 = SOCK_CLOSE_WAIT\r\n");
			close(SOCK_TCPS);
		  break;
	}
}

/**
*@brief		TCP Client回环演示函数。
*@param		无
*@return	无
*/
extern uint8_t local_ip[4];
extern char *Sn_SR_values[0x60];
//uint8 buff[100];


uint8_t do_tcp_client(void)
{	
//   uint16_t len=0,i=0;	
  uint16_t ret=0;
  ret=getSn_SR(SOCK_TCPC);//读取专用于TCP的Socket的状态值
  printf(" getSn_SR = 0x%X\r\n",ret);
//  printf(" socket状态 = %s\r\n",Sn_SR_values[ret]);
  switch(ret)								  				         /*判断socket的状态*/
  {
  case SOCK_CLOSED:											        		         /*如果socket处于关闭状态=0x00*/
    printf(" socket的状态 = SOCK_CLOSED\r\n");
    socket(SOCK_TCPC,Sn_MR_TCP,local_port,Sn_MR_ND);       //先将该 专用于TCP的Socket 初始化
    break;
    
  case SOCK_INIT:												/*如果socket处于初始化状态=0x13*/
    printf(" socket的状态 = SOCK_INIT\r\n");
    getSIPR (local_ip);			
    printf(" W5500 IP地址: %d.%d.%d.%d\r\n", local_ip[0], local_ip[1], local_ip[2], local_ip[3]);
    printf(" 服务器IP地址: %d.%d.%d.%d：%d\r\n",remote_ip[0],remote_ip[1],remote_ip[2],remote_ip[3],remote_port); 
    connect(SOCK_TCPC,remote_ip,remote_port);                /*socket连接服务器*/ 
    break;
    
  case SOCK_ESTABLISHED: 								/*如果socket处于连接建立状态=0x17， 完成3次握手完成后，才会进入建立连接状态*/
    printf(" socket的状态 = SOCK_ESTABLISHED\r\n"); 
    return 1;
    
  case SOCK_CLOSE_WAIT: 								 /*如果socket处于等待关闭状态=SOCK_CLOSE_WAIT*/
    printf(" socket的状态 = SOCK_CLOSE_WAIT\r\n");
    close(SOCK_TCPC); 							   	/**@Q：socket处于等待关闭状态而不是关闭状态*/
    break;
    
  }
  return 0;
}

