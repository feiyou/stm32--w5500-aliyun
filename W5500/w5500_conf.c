/*
**************************************************************************************************
* @file    		w5500_conf.c
* @author  		WIZnet Software Team 
* @version 		V1.0
* @date    		2015-02-14
* @brief  		配置MCU，移植W5500程序需要修改的文件，配置W5500的MAC和IP地址
**************************************************************************************************
*/
#include <stdio.h> 
#include <string.h>

#include "w5500_conf.h"
#include "utility.h"
#include "w5500.h"
#include "dhcp.h"
#include "spi.h"
#include "tim.h"
#include "main.h"
CONFIG_MSG  ConfigMsg;																	/*配置结构体*/
EEPROM_MSG_STR EEPROM_MSG;															/*EEPROM存储信息结构体*/

/*定义MAC地址,如果多块W5500网络适配板在同一现场工作，请使用不同的MAC地址*/
uint8_t mac[6]={0x00,0x08,0xdc,0x11,0x11,0x11};

/*定义默认IP信息*/
uint8_t local_ip[4]  ={192,168,2,88};											/*定义W5500默认IP地址*/
uint8_t subnet[4]    ={255,255,255,0};										/*定义W5500默认子网掩码*/
uint8_t gateway[4]   ={192,168,2,1};											/*定义W5500默认网关*/
uint8_t dns_server[4]={114,114,114,114};									/*定义W5500默认DNS*/

uint16_t local_port=5000;	                       					/*定义本地端口*/

/*定义远端IP信息*/
//uint8_t  remote_ip[4]={192,168,2,49};											/*远端IP地址——服务器的IP*/
//uint16_t remote_port=6000;																/*远端端口号*/
uint8_t  remote_ip[4]={47,102,164,191};											/*远端IP地址——服务器的IP*/
uint16_t remote_port=1883;			

/*IP配置方法选择，请自行选择*/
uint8_t	ip_from=IP_FROM_DEFINE;				

uint8_t   dhcp_ok   = 0;													   			/*dhcp成功获取IP*/
uint32_t	ms        = 0;															  	/*毫秒计数*/
uint32_t	dhcp_time = 0;															  	/*DHCP运行计数*/
uint8_t	    ntptimer  = 0;															  	/*NPT秒计数*/

/**
*@brief		配置W5500的IP地址
*@param		无
*@return	无
*/
void set_w5500_ip(void)
{	
  memcpy(ConfigMsg.mac, mac, 6);
  
  if(ip_from==IP_FROM_DEFINE)	
  {
    /*复制定义的配置信息到配置结构体*/		
    memcpy(ConfigMsg.lip,local_ip,4);
    memcpy(ConfigMsg.sub,subnet,4);
    memcpy(ConfigMsg.gw,gateway,4);
    memcpy(ConfigMsg.dns,dns_server,4);
    printf(" 使用定义的IP信息配置W5500\r\n");
    
  }
  
  /*使用EEPROM存储的IP参数*/	
  if(ip_from==IP_FROM_EEPROM)
  {
    while(1)
    {
      delay_ms(500);
      printf(" ip_from==IP_FROM_EEPROM \r\n");
    }
    
    //		/*从EEPROM中读取IP配置信息*/
    //		read_config_from_eeprom();		
    //		
    //		/*如果读取EEPROM中MAC信息,如果已配置，则可使用*/		
    //		if( *(EEPROM_MSG.mac)==0x00&& *(EEPROM_MSG.mac+1)==0x08&&*(EEPROM_MSG.mac+2)==0xdc)		
    //		{
    //			printf(" IP from EEPROM\r\n");
    //			/*复制EEPROM配置信息到配置的结构体变量*/
    //			memcpy(ConfigMsg.lip,EEPROM_MSG.lip, 4);				
    //			memcpy(ConfigMsg.sub,EEPROM_MSG.sub, 4);
    //			memcpy(ConfigMsg.gw, EEPROM_MSG.gw, 4);
    //		}
    //		else
    //		{
    //			printf(" EEPROM未配置,使用定义的IP信息配置W5500,并写入EEPROM\r\n");
    //			write_config_to_eeprom();	/*使用默认的IP信息，并初始化EEPROM中数据*/
    //		}			
  }
  
  /*使用DHCP获取IP参数，需调用DHCP子函数*/		
  if(ip_from==IP_FROM_DHCP)								
  {
    /*复制DHCP获取的配置信息到配置结构体*/
    if(dhcp_ok==1)
    {
      printf(" IP from DHCP\r\n");		 
      memcpy(ConfigMsg.lip,DHCP_GET.lip, 4);
      memcpy(ConfigMsg.sub,DHCP_GET.sub, 4);
      memcpy(ConfigMsg.gw,DHCP_GET.gw, 4);
      memcpy(ConfigMsg.dns,DHCP_GET.dns,4);
    }
    else
    {
      printf(" DHCP子程序未运行,或者不成功\r\n");
      printf(" 使用定义的IP信息配置W5500\r\n");
    }
  }
  
  /*以下配置信息，根据需要选用*/	
  ConfigMsg.sw_ver[0]=FW_VER_HIGH;
  ConfigMsg.sw_ver[1]=FW_VER_LOW;	
  
  /*将IP配置信息写入W5500相应寄存器*/	
  setSUBR(ConfigMsg.sub);
  setGAR(ConfigMsg.gw);
  setSIPR(ConfigMsg.lip);
  set_w5500_mac();//一直连不上，抓包结果显示ARP一直在询问服务器在哪，socket一直是closed
  
  getSIPR (local_ip);			
  printf(" W5500 IP地址   : %d.%d.%d.%d\r\n", local_ip[0],local_ip[1],local_ip[2],local_ip[3]);
  getSUBR(subnet);
  printf(" W5500 子网掩码 : %d.%d.%d.%d\r\n", subnet[0],subnet[1],subnet[2],subnet[3]);
  getGAR(gateway);
  printf(" W5500 网关     : %d.%d.%d.%d\r\n", gateway[0],gateway[1],gateway[2],gateway[3]);
}

/**
*@brief		配置W5500的MAC地址
*@param		无
*@return	无
*/
void set_w5500_mac(void)
{
  memcpy(ConfigMsg.mac, mac, 6);
  setSHAR(ConfigMsg.mac);	/**/
  memcpy(DHCP_GET.mac, mac, 6);
}

/**
*@brief		配置W5500的GPIO接口
*@param		无
*@return	无
*/
void gpio_for_w5500_config(void)
{
  //  SPI_InitTypeDef  SPI_InitStructure;
  //  GPIO_InitTypeDef GPIO_InitStructure;
  //	
  //  RCC_APB2PeriphClockCmd(WIZ_SPIx_RESET_CLK|WIZ_SPIx_INT_CLK, ENABLE);
  //	
  //  /* Enable SPI1 and GPIO clocks */
  //  /*!< SPI_FLASH_SPI_CS_GPIO, SPI_FLASH_SPI_MOSI_GPIO, 
  //       SPI_FLASH_SPI_MISO_GPIO, SPI_FLASH_SPI_DETECT_GPIO 
  //       and SPI_FLASH_SPI_SCK_GPIO Periph clock enable */
  //  RCC_APB2PeriphClockCmd(WIZ_SPIx_GPIO_CLK|WIZ_SPIx_SCS_CLK, ENABLE);
  //
  //  /*!< SPI_FLASH_SPI Periph clock enable */
  //  WIZ_SPIx_CLK_CMD(WIZ_SPIx_CLK, ENABLE);
  // 
  //  
  //  /*!< Configure SPI_FLASH_SPI pins: SCK */
  //  GPIO_InitStructure.GPIO_Pin = WIZ_SPIx_SCLK;
  //  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  //  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
  //  GPIO_Init(WIZ_SPIx_GPIO_PORT, &GPIO_InitStructure);
  //
  //  /*!< Configure SPI_FLASH_SPI pins: MISO */
  //  GPIO_InitStructure.GPIO_Pin = WIZ_SPIx_MISO;
  //  GPIO_Init(WIZ_SPIx_GPIO_PORT, &GPIO_InitStructure);
  //
  //  /*!< Configure SPI_FLASH_SPI pins: MOSI */
  //  GPIO_InitStructure.GPIO_Pin = WIZ_SPIx_MOSI;
  //  GPIO_Init(WIZ_SPIx_GPIO_PORT, &GPIO_InitStructure);
  //
  //  /*!< Configure SPI_FLASH_SPI_CS_PIN pin: SPI_FLASH Card CS pin */
  //  GPIO_InitStructure.GPIO_Pin = WIZ_SPIx_SCS;
  //  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
  //  GPIO_Init(WIZ_SPIx_SCS_PORT, &GPIO_InitStructure);
  //
  //
  //  /* SPI1 configuration */
  //  SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
  //  SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
  //  SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
  //  SPI_InitStructure.SPI_CPOL = SPI_CPOL_High;
  //  SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge;
  //  SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
  //  SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_4;
  //  SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
  //  SPI_InitStructure.SPI_CRCPolynomial = 7;
  //  SPI_Init(WIZ_SPIx, &SPI_InitStructure);
  //  SPI_Cmd(WIZ_SPIx, ENABLE);
  //	
  //  /*定义RESET引脚*/
  //  GPIO_InitStructure.GPIO_Pin = WIZ_RESET;					       /*选择要控制的GPIO引脚*/		 
  //  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;		     /*设置引脚速率为50MHz */		
  //  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;		     /*设置引脚模式为通用推挽输出*/	
  //  GPIO_Init(WIZ_SPIx_RESET_PORT, &GPIO_InitStructure);		 /*调用库函数，初始化GPIO*/
  //  GPIO_SetBits(WIZ_SPIx_RESET_PORT, WIZ_RESET);		
  //  /*定义INT引脚*/	
  //  GPIO_InitStructure.GPIO_Pin = WIZ_INT;						       /*选择要控制的GPIO引脚*/		 
  //  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;		     /*设置引脚速率为50MHz*/		
  //  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;				     /*设置引脚模式为通用推挽模拟上拉输入*/		
  //  GPIO_Init(WIZ_SPIx_INT_PORT, &GPIO_InitStructure);			 /*调用库函数，初始化GPIO*/
}

/**
*@brief		W5500片选信号设置函数
*@param		val: 为“0”表示片选端口为低，为“1”表示片选端口为高
*@return	无
*/
void wiz_cs(uint8_t val)
{
  if (val == LOW) 
  {
    HAL_GPIO_WritePin(WIZ_SPIx_SCS_PORT, WIZ_SPIx_SCS,GPIO_PIN_RESET); 
  }
  else if (val == HIGH)
  {
    HAL_GPIO_WritePin(WIZ_SPIx_SCS_PORT, WIZ_SPIx_SCS,GPIO_PIN_SET); 
  }
}

/**
*@brief		设置W5500的片选端口SCSn为低
*@param		无
*@return	无
*/
void iinchip_csoff(void)
{
  wiz_cs(LOW);
}

/**
*@brief		设置W5500的片选端口SCSn为高
*@param		无
*@return	无
*/
void iinchip_cson(void)
{	
  wiz_cs(HIGH);
}

/**
*@brief		W5500复位设置函数
*@param		无
*@return	无
*/
void reset_w5500(void)
{
  HAL_GPIO_WritePin(WIZ_SPIx_RESET_PORT, WIZ_RESET,GPIO_PIN_RESET);
  delay_us(2); 
  HAL_GPIO_WritePin(WIZ_SPIx_RESET_PORT, WIZ_RESET,GPIO_PIN_SET);
  delay_ms(1600);
}

uint8_t SPI_SendByte(uint8_t byte)
{
  uint8_t Rxdata;
  HAL_SPI_TransmitReceive(&WIZ_hspix,&byte,&Rxdata,1, 1000);      
  return Rxdata;
}

/**
*@brief		STM32 SPI1读写8位数据
*@param		dat：写入的8位数据
*@return	无
*/
uint8_t  IINCHIP_SpiSendData(uint8_t dat)
{
  return(SPI_SendByte(dat));
}

/**
*@brief		写入一个8位数据到W5500的通用寄存器，以可变数据长度模式
*@param		addrbsb: 写入数据的地址
*@param   data：写入的8位数据
*@return	无
*/
void IINCHIP_WRITE( uint32_t addrbsb,  uint8_t data)
{
  iinchip_csoff();                              		
  IINCHIP_SpiSendData( (addrbsb & 0x00FF0000)>>16);	
  IINCHIP_SpiSendData( (addrbsb & 0x0000FF00)>> 8);
  IINCHIP_SpiSendData( (addrbsb & 0x000000F8) + 4);  //控制段8b=00：区域选择位 (BSB[4:0])=0——通用寄存器；读/写访问模式位=1——写；OM[1:0]=00——可变数据长度模式，
  IINCHIP_SpiSendData(data);                   
  iinchip_cson();                            
}

/**
*@brief		从W5500读出一个8位数据
*@param		addrbsb: 写入数据的地址
*@param   data：从写入的地址处读取到的8位数据
*@return	无
*/
uint8_t IINCHIP_READ(uint32_t addrbsb)
{
  uint8_t data = 0;
  iinchip_csoff();                            
  IINCHIP_SpiSendData( (addrbsb & 0x00FF0000)>>16);
  IINCHIP_SpiSendData( (addrbsb & 0x0000FF00)>> 8);
  IINCHIP_SpiSendData( (addrbsb & 0x000000F8))    ;
  data = IINCHIP_SpiSendData(0x00);            
  iinchip_cson();                               
  return data;    
}

/**
*@brief		向W5500写入len字节数据
*@param		addrbsb: 写入数据的地址
*@param   buf：写入字符串
*@param   len：字符串长度
*@return	len：返回字符串长度
*/
uint16_t wiz_write_buf(uint32_t addrbsb,uint8_t* buf,uint16_t len)
{
  uint16_t idx = 0;
  if(len == 0) printf("Unexpected2 length 0\r\n");
  iinchip_csoff();                               
  IINCHIP_SpiSendData( (addrbsb & 0x00FF0000)>>16);
  IINCHIP_SpiSendData( (addrbsb & 0x0000FF00)>> 8);
  IINCHIP_SpiSendData( (addrbsb & 0x000000F8) + 4); 
  for(idx = 0; idx < len; idx++)
  {
    IINCHIP_SpiSendData(buf[idx]);
  }
  iinchip_cson();                           
  return len;  
}

/**
*@brief		从W5500读出len字节数据
*@param		addrbsb: 读取数据的地址
*@param 	buf：存放读取数据
*@param		len：字符串长度
*@return	len：返回字符串长度
*/
uint16_t wiz_read_buf(uint32_t addrbsb, uint8_t* buf,uint16_t len)
{
  uint16_t idx = 0;
  if(len == 0)
  {
    printf("Unexpected2 length 0\r\n");
  }
  iinchip_csoff();                                
  IINCHIP_SpiSendData( (addrbsb & 0x00FF0000)>>16);
  IINCHIP_SpiSendData( (addrbsb & 0x0000FF00)>> 8);
  IINCHIP_SpiSendData( (addrbsb & 0x000000F8));    
  for(idx = 0; idx < len; idx++)                   
  {
    buf[idx] = IINCHIP_SpiSendData(0x00);
  }
  iinchip_cson();                                  
  return len;
}

/**
*@brief		写配置信息到EEPROM
*@param		无
*@return	无
*/
void write_config_to_eeprom(void)
{
//  uint16_t dAddr=0;
  //	ee_WriteBytes(ConfigMsg.mac,dAddr,(uint8_t)EEPROM_MSG_LEN);				
  delay_ms(10);																							
}

/**
*@brief		从EEPROM读配置信息
*@param		无
*@return	无
*/
void read_config_from_eeprom(void)
{
  //	ee_ReadBytes(EEPROM_MSG.mac,0,EEPROM_MSG_LEN);
  delay_us(10);
}

/**
*@brief		STM32定时器2初始化
*@param		无
*@return	无
*/
void timer2_init(void)
{
  //	MX_TIM2_Init();		// TIM2 定时配置 
  HAL_TIM_Base_Start_IT(&htim2);// 开启定时器3
}

/**
*@brief		dhcp用到的定时器初始化
*@param		无
*@return	无
*/
void dhcp_timer_init(void)
{
  timer2_init();																	
}

/**
*@brief		ntp用到的定时器初始化
*@param		无
*@return	无
*/
void ntp_timer_init(void)
{
  timer2_init();																	
}

/**
*@brief		定时器2中断函数
*@param		无
*@return	无
*/
void timer2_isr(void)
{
  ms++;	
  if(ms>=1000)
  {  
    HAL_GPIO_TogglePin(LED6_Y_GPIO_Port, LED6_Y_Pin);
    ms=0;
    dhcp_time++;																					/*DHCP定时加1S*/
#ifndef	__NTP_H__
    ntptimer++;																						/*NTP重试时间加1S*/
#endif
  }
  
}
/**
*@brief		STM32系统软复位函数
*@param		无
*@return	无
*/
void reboot(void)
{
  //  pFunction Jump_To_Application;
  //  uint32_t JumpAddress;
  //  printf(" 系统重启中……\r\n");
  //  JumpAddress = *(vu32*) (0x00000004);
  //  Jump_To_Application = (pFunction) JumpAddress;
  //  Jump_To_Application();
}

